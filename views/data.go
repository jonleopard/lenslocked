package views

const (
	AlertLvlError   = "red"
	AlertLvlWarning = "orange"
	AlertLvlInfo    = "blue"
	AlertLvlSuccess = "green"

	// AlertMsgGeneric is displayed when any random error is
	// encountered by our backend.
	AlertMsgGeneric = "Something went wrong. Pleae try and again and contact us if the problem persists."
)

// Alert is used to render Tailwind Alert messages in templates
type Alert struct {
	Level   string
	Message string
}

// Data is the top level structure that viewss expect data
// to come in.
type Data struct {
	Alert *Alert
	Yield interface{}
}

func (d *Data) SetAlert(err error) {
	if pErr, ok := err.(PublicError); ok {
		d.Alert = &Alert{
			Level:   AlertLvlError,
			Message: pErr.Public(),
		}
	} else {
		d.Alert = &Alert{
			Level:   AlertLvlError,
			Message: AlertMsgGeneric,
		}
	}
}

func (d *Data) AlertError(msg string) {
	d.Alert = &Alert{
		Level:   AlertLvlError,
		Message: AlertMsgGeneric,
	}
}

type PublicError interface {
	error
	Public() string
}
