package controllers

import "lenslocked.com/views"

func NewStatic() *Static {
	return &Static{
		Home:    views.NewView("tailwind", "static/home"),
		Contact: views.NewView("tailwind", "static/contact"),
	}
}

type Static struct {
	Home    *views.View
	Contact *views.View
}
